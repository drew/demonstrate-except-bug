# Demonstrate except bug

With the `branches` special keyword in the `only` list, naming a branch in the `except` list prevents that job from running in that branch. In this project, the `all-branches-except` job doesn't run in the `example-branch` branch.

However, with the `merge_requests` special keyword in the `only` list, naming a branch in the `except` list has no effect. In this project, the `all-merge-requests-except` job shoudn't run in the `example-branch` branch, but does anyway.

You can view proof of this happening in the [CI/CD](https://gitlab.com/wescossick/demonstrate-except-bug/pipelines) tab.